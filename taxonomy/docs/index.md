# Backstage taxonomy guidelines

This documentation aims to help the Backstage Initiative team, and the users, to define and understand the different objects of our Backstage data model.

It is also a guide to use the different values of some metadata like, for instance, [Type](type.md), [Tags](tags.md), [Lifecycle](lifecycle.md), ...

!!! warning
    It's an heavy work in progress.

## Overview of the model

![model](images/software-model-entities.svg)